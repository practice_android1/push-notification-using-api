package com.example.onesignalnotification;

import android.app.Application;

import com.onesignal.OneSignal;

public class Notification extends Application {
    private static final String ONESIGNAL_APP_ID = "fab16e8c-765f-4c54-9cb4-933b0b3e5d7c";
    @Override
    public void onCreate() {
        super.onCreate();
        // Enable verbose OneSignal logging to debug issues if needed.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        // OneSignal Initialization
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
        OneSignal.promptForPushNotifications();
    }
}
