package com.example.onesignalnotification;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.onesignalnotification.databinding.ActivityMainBinding;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private static final String ONESIGNAL_APP_ID = "fab16e8c-765f-4c54-9cb4-933b0b3e5d7c";
    private final String userId = "a5e07e6c-fa89-4f62-8d9d-d1f32970bf73";
    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //Method 1 using HttpURLConnecting
//        binding.btnSend.setOnClickListener(v -> {
//            try {
//                String jsonResponse;
//                URL url = new URL("https://onesignal.com/api/v1/notifications");
//                HttpURLConnection con = (HttpURLConnection)url.openConnection();
//                con.setUseCaches(false);
//                con.setDoOutput(true);
//                con.setDoInput(true);
//                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                con.setRequestProperty("Authorization", "Basic MTE4YTYxYmUtOTU0OC00YWMwLTkyNjMtY2QyYjllMmY5MTU0");
//                con.setRequestMethod("POST");
//                String strJsonBody = "{"
//                        +   "\"app_id\": \"fab16e8c-765f-4c54-9cb4-933b0b3e5d7c\","
//                        +   "\"included_segments\": [\"All\"],"
//                        +   "\"data\": {\"foo\": \"bar\"},"
//                        +   "\"contents\": {\"en\": \"Android is the popular subject!\"}"
//                        + "}";
//                System.out.println("strJsonBody:\n" + strJsonBody);
//                byte[] sendBytes = strJsonBody.getBytes(StandardCharsets.UTF_8);
//                con.setFixedLengthStreamingMode(sendBytes.length);
//                OutputStream outputStream = con.getOutputStream();
//                outputStream.write(sendBytes);
//                int httpResponse = con.getResponseCode();
//                System.out.println("httpResponse: " + httpResponse);
//                if (  httpResponse >= HttpURLConnection.HTTP_OK
//                        && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
//                    Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
//                    jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
//                    scanner.close();
//                }
//                else {
//                    Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
//                    jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
//                    scanner.close();
//                }
//                System.out.println("jsonResponse:\n" + jsonResponse);
//            } catch(Throwable t) {
//                t.printStackTrace();
//            }
//        });
        //Method 2 using OKHttpClient
        binding.btnSend.setOnClickListener(v -> {
            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("Content-Type, application/json; charset=utf-8");
            RequestBody body = RequestBody.create(mediaType, "{"
                        +   "\"app_id\": \"fab16e8c-765f-4c54-9cb4-933b0b3e5d7c\","
                        +   "\"included_segments\": [\"All\"],"
                        +   "\"data\": {\"foo\": \"bar\"},"
                        +   "\"contents\": {\"en\": \"Android is the popular subject!\"}"
                        + "}");
            Request request = new Request.Builder()
                    .url("https://onesignal.com/api/v1/notifications")
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic MTE4YTYxYmUtOTU0OC00YWMwLTkyNjMtY2QyYjllMmY5MTU0")
                    .post(body)
                    .build();

            try {
                Response response = client.newCall(request).execute();
                Log.d("TAG", "onCreate: " + response.message());
            } catch (IOException e) {
                e.printStackTrace();
            }
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        throw new IOException("Unexpected code " + response);
                    }
                }
            });
        });
    }
}